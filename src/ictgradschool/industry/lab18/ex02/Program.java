package ictgradschool.industry.lab18.ex02;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import java.util.*;

/**
 * TODO Have fun :)
 */
public class Program extends JFrame {

    private static final Random num = new Random();
    private Canvas gameScreen = new Canvas();
    private int newBlockX = -1, newBlockY = -1;
    private ArrayList<Integer> redX = new ArrayList<>();
    private ArrayList<Integer> snakeXLocation = new ArrayList<>();
    private ArrayList<Integer> redY = new ArrayList<>();
    private ArrayList<Integer> snakeYLocation = new ArrayList<>();
    private int direction;
    private boolean gameOver = false;

    public static void main(String[] args) {
        new Program().go();
    }

    public Program() {
        for (int i = 0; i < 6; i++) {
            snakeXLocation.add(10 - i);
            snakeYLocation.add(10);
        }
        this.direction = 39;
        setTitle("Program" + " : " + 6);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBounds(200, 200, 30 * 25 + 6, 20 * 25 + 28);
        setResizable(false);
        gameScreen.setBackground(Color.white);
        add(BorderLayout.CENTER, gameScreen);

        addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                int d1 = e.getKeyCode();
                if ((d1 >= 37) && (d1 <= 40)) {// block wrong codes
                    if (Math.abs(Program.this.direction - d1) != 2) {// block moving back
                        Program.this.direction = d1;
                    }
                }
            }
        });
        setVisible(true);
    }

    public void go() { // main loop
        while (!gameOver) {
            moveSnake();
            addNewBlock();
            gameScreen.repaint();
            try {
                Thread.sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void addNewBlock() {
        if (newBlockX == -1) {
            addNewGreenBlock();
            addNewRedBlock();
        }
    }

    public void addNewRedBlock() {
        int newRedX, newRedY;
        boolean redOnRed = false;
        boolean redOnSnake = false;
        do {
            newRedX = num.nextInt(30);
            newRedY = num.nextInt(20);
            redOnRed = checkBlockOnBlock(newRedX, newRedY, redOnRed);
            redOnSnake = checkBlockOnSnake(newRedX, newRedY, redOnSnake);
        } while (redOnRed || redOnSnake || newBlockX == newRedX && newBlockY == newRedY);

        redX.add(newRedX);
        redY.add(newRedY);
    }

    public void addNewGreenBlock() {
        int newGreenX, newGreenY;
        boolean greenOnRed = false;
        boolean greenOnSnake = false;
        do {
            newGreenX = num.nextInt(30);
            newGreenY = num.nextInt(20);
            greenOnRed = checkBlockOnBlock(newGreenX, newGreenY, greenOnRed);
            greenOnSnake = checkBlockOnSnake(newGreenX, newGreenY, greenOnSnake);
        } while (greenOnSnake || greenOnRed);
        newBlockX = newGreenX;
        newBlockY = newGreenY;
    }

    private boolean checkBlockOnSnake(int newGreenX, int newGreenY, boolean greenOnSnake) {
        for (int i = 0; i < this.snakeXLocation.size(); i++) {
            if ((this.snakeXLocation.get(i) == newGreenX) && (this.snakeYLocation.get(i) == newGreenY)) {
                if (!((this.snakeXLocation.get(this.snakeXLocation.size() - 1) == newGreenX) && (this.snakeYLocation.get(this.snakeYLocation.size() - 1) == newGreenY))) {
                    greenOnSnake = true;
                }
            }
        }
        return greenOnSnake;
    }

    private boolean checkBlockOnBlock(int newGreenX, int newGreenY, boolean greenOnRed) {
        for (int i = 0; i < redX.size(); i++) {
            if (redX.get(i) == newGreenX && redY.get(i) == newGreenY) {
                greenOnRed = true;
            }
        }
        return greenOnRed;
    }

    public void moveSnake() {
        int snakeXMove = this.snakeXLocation.get(0);
        int snakeYMove = this.snakeYLocation.get(0);
        if (direction == 37) {
            snakeXMove--;
        }
        if (direction == 39) {
            snakeXMove++;
        }
        if (direction == 38) {
            snakeYMove--;
        }
        if (direction == 40) {
            snakeYMove++;
        }
        if (snakeXMove > 30 - 1) {
            snakeXMove = 0;
        }
        if (snakeXMove < 0) {
            snakeXMove = 30 - 1;
        }
        if (snakeYMove > 20 - 1) {
            snakeYMove = 0;
        }
        if (snakeYMove < 0) {
            snakeYMove = 20 - 1;
        }
        checkCollisions(snakeXMove, snakeYMove);
        this.snakeXLocation.add(0, snakeXMove);
        this.snakeYLocation.add(0, snakeYMove);
        checkSnakeGrow();
    }

    public void checkSnakeGrow() {
        if (((this.snakeXLocation.get(0) == newBlockX) && (this.snakeYLocation.get(0) == newBlockY))) {
            newBlockX = -1;
            newBlockY = -1;
            setTitle("Program" + " : " + this.snakeXLocation.size());
        } else {
            this.snakeXLocation.remove(this.snakeXLocation.size() - 1);
            this.snakeYLocation.remove(this.snakeYLocation.size() - 1);
        }
    }

    public void checkCollisions(int snakeXMove, int snakeYMove) {
        boolean redCollide = checkSnakeCollideWithRed(snakeXMove, snakeYMove);
        boolean selfCollision = checkCollideWithSelf(snakeXMove, snakeYMove);
        gameOver = redCollide || selfCollision;
    }

    public boolean checkCollideWithSelf(int snakeXMove, int snakeYMove) {
        boolean selfCollision = false;
        for (int i1 = 0; i1 < this.snakeXLocation.size(); i1++) {
            if ((this.snakeXLocation.get(i1) == snakeXMove) && (this.snakeYLocation.get(i1) == snakeYMove)) {
                if (!((this.snakeXLocation.get(this.snakeXLocation.size() - 1) == snakeXMove) && (this.snakeYLocation.get(this.snakeYLocation.size() - 1) == snakeYMove))) {
                    selfCollision = true;
                    System.out.println("selfcollide");
                }
            }
        }
        return selfCollision;
    }

    public boolean checkSnakeCollideWithRed(int snakeXMove, int snakeYMove) {
        boolean redCollide = false;

        for (int i1 = 0; i1 < redX.size(); i1++) {
            if (redX.get(i1) == snakeXMove && redY.get(i1) == snakeYMove) {
                redCollide = true;
                System.out.println("redcollide");
            }
        }
        return redCollide;
    }

    private class Canvas extends JPanel {
        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            for (int i = 0; i < snakeXLocation.size(); i++) {
                g.setColor(Color.gray);
                g.fill3DRect(snakeXLocation.get(i) * 25 + 1, snakeYLocation.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            g.setColor(Color.green);
            g.fill3DRect(newBlockX * 25 + 1, newBlockY * 25 + 1, 25 - 2, 25 - 2, true);
            for (int i = 0; i < redX.size(); i++) {
                g.setColor(Color.red);
                g.fill3DRect(redX.get(i) * 25 + 1, redY.get(i) * 25 + 1, 25 - 2, 25 - 2, true);
            }
            if (gameOver) {
                g.setColor(Color.red);
                g.setFont(new Font("Arial", Font.BOLD, 60));
                FontMetrics fm = g.getFontMetrics();
                g.drawString("Over", (30 * 25 + 6 - fm.stringWidth("Over")) / 2, (20 * 25 + 28) / 2);
            }
        }
    }
}