package ictgradschool.industry.lab18.ex01;

import java.io.*;
import java.util.*;


public class ExcelNew {

	public static void main(String [] args){
		ExcelNew ex = new ExcelNew();
		ex.start();

	}

	public void start(){
			String output;
			ArrayList<String> firstNameList = readFile("FirstNames.txt");
			ArrayList<String> surnameList = readFile("Surnames.txt");
			output = createOutput(firstNameList, surnameList);
			writeFile(output);
	}

	public void writeFile(String output){
		try {
			BufferedWriter bw = new BufferedWriter(new FileWriter("Data_Out.txt"));
			bw.write(output);
			bw.close();
		}catch(IOException e){
			System.out.println(e);
		}
	}

	public String createOutput(ArrayList<String> firstNameList, ArrayList<String> surnameList) {
		int classSize = 550;
		String output = "";
		for(int i = 1; i <= classSize; i++){
            String student = "";
            if(i/10 < 1){
                student += "000" + i;
            }else if (i/100 < 1){
                student += "00" + i;
            }else if (i/1000 < 1){
                student += "0"+i;
            }else{
                student += i;
            }
            student = getStudentName(firstNameList, surnameList, student);
            //Student Skill
            int randStudentSkill = (int)(Math.random()*101);
            //Labs//////////////////////////
            student = getLabs(student, randStudentSkill);
            //Test/////////////////////////
            student = getTest(student, randStudentSkill, 20, 65, 90);
            ///////////////Exam////////////
            student = getExam(student, randStudentSkill);
            output += student;
        }
		return output;
	}

	public String getStudentName(ArrayList<String> firstNameList, ArrayList<String> surnameList, String student) {
		int randFNIndex = (int)(Math.random()*firstNameList.size());
		int randSNIndex = (int)(Math.random()*surnameList.size());
		student += "\t" + surnameList.get(randSNIndex) + "\t" + firstNameList.get(randFNIndex) + "\t";
		return student;
	}

	public ArrayList<String> readFile(String myFile) {
		String line;
		ArrayList<String> firstNameList = new ArrayList<String>();
		try {
			BufferedReader br = new BufferedReader(new FileReader(myFile));
			while ((line = br.readLine()) != null) {
				firstNameList.add(line);
			}
			br.close();
		return firstNameList;
		} catch(IOException e){
			System.out.println(e);
		}
		return null;
	}

	public String getExam(String student, int randStudentSkill) {
		if(randStudentSkill <= 7){
            int randDNSProb = (int)(Math.random()*101);
            if(randDNSProb <= 5){
                student += ""; //DNS
            }else{
                student += (int)(Math.random()*40); //[0,39]
            }
        } else if(randStudentSkill <= 20){
                student += ((int)(Math.random()*10) + 40); //[40,49]
        } else if(randStudentSkill <= 60){
            student += ((int)(Math.random()*20) + 50);//[50,69]
        } else if(randStudentSkill <= 90){
            student += ((int)(Math.random()*20) + 70); //[70,89]
        } else{
            student += ((int)(Math.random()*11) + 90); //[90,100]
        }
		//////////////////////////////////
		student += "\n";
		return student;
	}

	public String getLabs(String student, int randStudentSkill) {
		int numLabs = 3;
		for(int j = 0; j < numLabs; j++){
            student = getTest(student, randStudentSkill, 15, 25, 65);
        }
		return student;
	}

	public String getTest(String student, int randStudentSkill, int i2, int i3, int i4) {
		if (randStudentSkill <= 5) {
			student += (int) (Math.random() * 40); //[0,39]
		} else if (randStudentSkill <= i2) {
			student += ((int) (Math.random() * 10) + 40); //[40,49]
		} else if (randStudentSkill <= i3) {
			student += ((int) (Math.random() * 20) + 50); //[50,69]
		} else if (randStudentSkill <= i4) {
			student += ((int) (Math.random() * 20) + 70); //[70,89]
		} else {
			student += ((int) (Math.random() * 11) + 90); //[90,100]
		}
		student += "\t";
		return student;
	}

}